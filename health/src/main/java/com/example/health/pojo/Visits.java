package com.example.health.pojo;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name="visits")
public class Visits {
	
	@Id
	@GeneratedValue
	private	int id;
	private Date date;
	@OneToOne
	@JoinColumn(name="vapid")
	private Apoinment apoinment  ;
	@OneToOne
	@JoinColumn(name="vdpid")
	private  Doctor doctor ;
	
	
	public Apoinment getApoinment() {
		return apoinment;
	}
	public void setApoinment(Apoinment apoinment) {
		this.apoinment = apoinment;
	}
	public Doctor getDoctor() {
		return doctor;
	}
	public void setDoctor(Doctor doctor) {
		this.doctor = doctor;
	}
	public int getId() {
		return id;
		
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	@Override
	public String toString() {
		return "Visits [id=" + id + ", date=" + date + ", apoinment=" + apoinment + ", doctor=" + doctor + "]";
	}
	

}
