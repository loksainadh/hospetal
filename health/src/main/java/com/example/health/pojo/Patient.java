package com.example.health.pojo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="patient")
public class Patient {
	@Id
	@GeneratedValue
 private  int id;
 private  String name;
 private  int age;
 private  String gender;
 private  String addr;
 private    long num;
 
@Override
public String toString() {
	return "Patient [id=" + id + ", name=" + name + ", age=" + age + ", gender=" + gender + ", addr=" + addr + ", num="
			+ num + "]";
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getAge() {
	return age;
}
public void setAge(int age) {
	this.age = age;
}
public String getGender() {
	return gender;
}
public void setGender(String gender) {
	this.gender = gender;
}
public String getAddr() {
	return addr;
}
public void setAddr(String addr) {
	this.addr = addr;
}
public long getNum() {
	return num;
}
public void setNum(long num) {
	this.num = num;
	
}
}
