package com.example.health.pojo;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table
public class Complaint {
	
	@Id
	private int id;
	private Date duration;
	private String comments;
	private String vvcondition;
	@ManyToOne
	@JoinColumn(name="vid")
	private Visits  visits;
	@ManyToOne
	@JoinColumn(name="did")
	private Desease desease;
	 
	
	@Override
	public String toString() {
		return "Complaint [id=" + id + ", duration=" + duration + ", comments=" + comments + ", vcondition="
				+ vvcondition + ", visits=" + visits + ", desease=" + desease + "]";
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getDuration() {
		return duration;
	}
	public void setDuration(Date duration) {
		this.duration = duration;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getVcondition() {
		return vvcondition;
	}
	public void setVcondition(String vcondition) {
		this.vvcondition = vcondition;
	}
	public Visits getVisits() {
		return visits;
	}
	public void setVisits(Visits visits) {
		this.visits = visits;
	}
	public Desease getDesease() {
		return desease;
	}
	public void setDesease(Desease desease) {
		this.desease = desease;
	}
	
	
		
	}


