package com.example.health.pojo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name="apointment")
public class Apoinment {
	@Id
	@GeneratedValue
	private int id;
	private Date date;
	 private int duretion;
	private String healthissue;
	@ManyToOne
	@JoinColumn(name = "apid")
	private Patient patient;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHealthissue() {
		return healthissue;
	}

	public void setHealthissue(String healthissue) {
		this.healthissue = healthissue;
	}

	public int getDuretion() {
		return duretion;
	}

	public void setDuretion(int duretion) {
		this.duretion = duretion;
	}

	
	public Patient getPatient() {
		return patient;
	}

	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	

	@Override
	public String toString() {
		return "Apoinment [id=" + id + ", date=" + date + ", healthissue=" + healthissue + ", patient=" + patient
				+ ", duretion=" + duretion + "]";
	}
	
}
