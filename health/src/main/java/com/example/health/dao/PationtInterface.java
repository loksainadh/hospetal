package com.example.health.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.health.exptionhandler.Applicationexception;
import com.example.health.pojo.Patient;
@Repository
public interface PationtInterface extends JpaRepository<Patient, Integer>{
	
	@Query("select d.name from Visits v join v.apoinment a join a.patient p join v.doctor d where p.name=?1")
	public List<String> getdocternamebygivingpatientname(String name)throws Applicationexception;
	
	
}
