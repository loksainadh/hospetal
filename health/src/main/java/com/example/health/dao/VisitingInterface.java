package com.example.health.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.health.pojo.Visits;
@Repository
public interface VisitingInterface  extends JpaRepository<Visits, Integer>{

	

}
