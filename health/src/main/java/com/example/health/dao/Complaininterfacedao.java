package com.example.health.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.health.pojo.Complaint;

public interface Complaininterfacedao extends JpaRepository<Complaint, Integer> {

}
